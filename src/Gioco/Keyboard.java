package Gioco;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

public class Keyboard implements KeyListener {

	@Override
	public void keyPressed(KeyEvent e) {
		if(Main.scene.mario.isAlive() == true){
			if(e.getKeyCode() == KeyEvent.VK_RIGHT){
				if(Main.scene.getxPos() == -1){
					Main.scene.setxPos(0);
					Main.scene.setX1(-50);
					Main.scene.setX2(750);
				}
				Main.scene.mario.setMoving(true);
				Main.scene.mario.setMovingRight(true);
				Main.scene.setMov(1);
			} else if(e.getKeyCode() == KeyEvent.VK_LEFT){
				if(Main.scene.getxPos() == 4601){
					Main.scene.setxPos(4600);
					Main.scene.setX1(-50);
					Main.scene.setX2(750);
				}
				Main.scene.mario.setMoving(true);
				Main.scene.mario.setMovingRight(false);
				Main.scene.setMov(-1);
			}
			if(e.getKeyCode() == KeyEvent.VK_UP){
				Main.scene.mario.setJumping(true);
				Audio.playSound("/audio/jump.wav");
			}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		Main.scene.mario.setMoving(false);
		Main.scene.setMov(0);
	}

	@Override
	public void keyTyped(KeyEvent e) {}

}
