package GameComponent;

import Gioco.Main;

public class Component implements GameComponent {

    private int width;
    private int height;
    private int x;
    private int y;

    public Component(final int x, final int y, final int width, final int height){
        this.x = x;
        this.y = y;
        this.width = width;
        this.height= height;
    }

    @Override
    public int getWidth() {
        return this.width;
    }

    @Override
    public int getHeight() {
        return this.height;
    }

    @Override
    public int getX() {
        return this.x;
    }

    @Override
    public int getY() {
        return this.y;
    }

    @Override
    public void setX(final int x) {
        this.x = x;
    }

    @Override
    public void setY(final int y) {
        this.y = y;
    }

    @Override
    public void shift(){
        if(Main.scene.getxPos() >= 0){
            this.x = this.x - Main.scene.getMov();
        }
    }

}
