package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

import GameComponent.GameComponent;
import GameComponent.GameObject.GameObject;
import GameComponent.GameObject.GameObjectInterchangeable;
import Gioco.Main;
import GameComponent.GameObject.GameObjectImpl;
import GameComponent.GameObject.Coin;

public class Mario extends CharacterImpl {

	private ImageIcon icon;
	private Image image;
	private boolean isJumping;
	private int jumpIntensity;
	
	public Mario(final int x, final int y) {
		super(x, y, 28 , 50);
		icon = new ImageIcon(getClass().getResource("/imagine/marioD.png"));
		this.image = this.icon.getImage();
		this.isJumping = false;
		this.jumpIntensity = 0;
	}

	public boolean isJumping() {
		return this.isJumping;
	}

	public void setJumping(final boolean jumping) {
		this.isJumping = jumping;
	}

	public Image getImage() {
		return this.image;
	}

	@Override
	public Image changeImage(final String name, final int frequency){
		String pathImage;
		ImageIcon icon;
		Image image;
		if(this.isMoving() == false || Main.scene.getxPos() <=0 || Main.scene.getxPos() > 4600){
			if(this.isMovingRight() == true){
				pathImage = "/imagine/" + name + "AD.png";
			} else {
				pathImage = "/imagine/" + name + "AG.png";
			}
		} else {
			this.setCounter(this.getCounter()+1);
			if(this.getCounter() / frequency == 0){
				if(this.isMovingRight() == true){
					pathImage = "/imagine/" + name + "AD.png";
				} else {
					pathImage = "/imagine/" + name + "AG.png";
				}
			} else {
				if(this.isMovingRight() == true){
					pathImage = "/imagine/" + name + "D.png";
				} else {
					pathImage = "/imagine/" + name + "G.png";
				}
			}
			if(this.getCounter() == 2 * frequency)
				this.setCounter(0);
		}
		icon = new ImageIcon(getClass().getResource(pathImage));
		image = icon.getImage();
		return image;
	}
	
	public Image jumping(){
		ImageIcon icon;
		Image image;
		String pathImage;
		this.jumpIntensity++;
		if(this.jumpIntensity <= 41){
			if(this.getY() > Main.scene.getHauteurPlafond()) {
				this.setY(this.getY() - 4);
			} else {
				this.jumpIntensity = 42;
			}
			if(this.isMovingRight() == true) {
				pathImage = "/imagine/marioSD.png";
			} else {
				pathImage = "/imagine/marioSG.png";
			}
		} else if (this.getY() + this.getHeight() < Main.scene.getySol()){
			this.setY(this.getY() + 1);
			if(this.isMovingRight() == true) {
				pathImage = "/imagine/marioSD.png";
			} else {
				pathImage = "/imagine/marioSG.png";
			}
		} else {
			if(this.isMovingRight() == true) {
				pathImage = "/imagine/marioAD.png";
			} else {
				pathImage = "/imagine/marioAG.png";
			}
			this.isJumping = false;
			this.jumpIntensity = 0 ;
		}
		icon = new ImageIcon(getClass().getResource(pathImage));
		image = icon.getImage();
		return image ;
	}
	
	public void contactWithComponent(final GameComponent component){
		if(component instanceof Character) {
			Character character = (Character) component;
			if((super.isMovingRight(character) == true) || (super.isMovingLeft(character) == true)){
				if(character.isAlive() == true){
					this.setMoving(false);
					this.setAlive(false);
				} else {
					this.setAlive(true);
				}
			} else if(super.isMovingDown(component) == true){
				character.setMoving(false);
				character.setAlive(false);
			}
		}
		if(super.isMovingRight(component)== true && this.isMovingRight() == true || (super.isMovingLeft(component)==true) && this.isMovingRight() == false ){
			Main.scene.setMov(0);
			this.setMoving(false);
		}
		if(super.isMovingDown(component)==true && this.isJumping == true) {
			Main.scene.setySol((int) component.getY());
		} else if (super.isMovingDown(component)==false){
			Main.scene.setySol(293);
			if(this.isJumping == false){
				this.setY(243);
			}
			if(this.isMovingUp(component) == true){
				Main.scene.setHauteurPlafond((int) (component.getY() + component.getHeight()));
			} else if (super.isMovingUp(component) == false && this.isJumping == false){
				Main.scene.setHauteurPlafond(0);
			}
		}
	}
	
	public boolean isInContactWithANInterchangeableObject(final GameObjectInterchangeable objectInterchangeable){
		if(this.isMovingLeft(objectInterchangeable) == true || this.isMovingUp(objectInterchangeable) == true || this.isMovingRight(objectInterchangeable)==true ||this.isMovingDown(objectInterchangeable) == true){
			return true;
		}
		return false;
	}

}
