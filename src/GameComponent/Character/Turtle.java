package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

import GameComponent.GameComponent;

public class Turtle extends DeadlyCharacter {

	public Turtle(final int x, final int y) {
		super(x, y, 43, 50, "/imagine/turtleAD.png");
		
		Thread chronoTurtle = new Thread(this);
		chronoTurtle.start();
	}
		 
	public Image changeImageAfterDeath(){
			String pathImage = "/imagine/turtleF.png";
			ImageIcon icon;
			Image image;
	        icon = new ImageIcon(getClass().getResource(pathImage));
	        image = icon.getImage();
			return image;
	}
}