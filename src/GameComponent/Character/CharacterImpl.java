package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

import GameComponent.Component;
import GameComponent.GameComponent;
import GameComponent.GameObject.GameObject;
import GameComponent.GameObject.GameObjectImpl;
import Gioco.Main;

public abstract class CharacterImpl extends Component implements Character{

	private boolean isMoving;
	private boolean isMovingRight;
	private int counter;
	private boolean isAlive;

	public CharacterImpl(final int x, final int y, final int width, final int height){
		super(x, y, width, height);

		this.counter = 0;
		this.isMoving = false ;
		this.isMovingRight = false ;
		this.isAlive = true;
	}

	@Override
	public int getCounter() {
		return this.counter;
	}

	@Override
	public void setAlive(final boolean isAlive) {
		this.isAlive = isAlive;
	}

	@Override
	public void setMoving(final boolean isMoving) {
		this.isMoving = isMoving;
	}

	@Override
	public void setMovingRight(final boolean isMovingRight) {
		this.isMovingRight = isMovingRight;
	}

	@Override
	public void setCounter(final int counter) {
		this.counter = counter;
	}

	@Override
	public boolean isAlive() {
		return isAlive;
	}

	@Override
	public boolean isMoving() {
		return this.isMoving;
	}

	@Override
	public boolean isMovingRight() {
		return this.isMovingRight;
	}

	@Override
	public Image changeImage(final String name, final int frequency){
		String pathImage;
		ImageIcon icon;
		Image image;
		if(this.isMoving == false ){
			if(this.isMovingRight == true){
				pathImage = "/imagine/" + name + "AD.png";
			} else {
				pathImage = "/imagine/" + name + "AG.png";
			}
		} else {
			this.counter++;
			if(this.counter / frequency == 0){
				if(this.isMovingRight == true){
					pathImage = "/imagine/" + name + "AD.png";
				} else {
					pathImage = "/imagine/" + name + "AG.png";
				}
			} else {
				if(this.isMovingRight == true){
					pathImage = "/imagine/" + name + "D.png";
				} else {
					pathImage = "/imagine/" + name + "G.png";
				}
			}
			if(this.counter == 2 * frequency ) {
				this.counter = 0;
			}
		}
		icon = new ImageIcon(getClass().getResource(pathImage));
		image = icon.getImage();
		return image;
	}

	@Override
	public boolean isCloseToAComponent(final GameComponent component){
		if((this.getX() > component.getX() - 10 && this.getX()< component.getX() + component.getWidth() + 10) || (this.getX()+ this.getWidth() > component.getX() - 10 && this.getX() + this.getWidth() < component.getX() + component.getWidth() + 10)) {
			return true;
		} else {
			return false ;
		}
	}

	public abstract void contactWithComponent(final GameComponent component);

	protected boolean isMovingRight(final GameComponent component){
		if (component instanceof Character && this.isMovingRight() == false){
			return false;
		}
		if(this.getX() + this.getWidth() < component.getX() || this.getX() + this.getWidth() > component.getX() + 5 || this.getY() + this.getHeight() <= component.getY() || this.getY() >= component.getY() + component.getHeight() ){
			return false;
		}
		return true;
	}

	protected boolean isMovingLeft(final GameComponent component){
		if(this.getX() > component.getX() + component.getWidth() ||this.getX() + this.getWidth() < component.getX() + component.getWidth() -5 || this.getY() + this.getHeight() <= component.getY() || this.getY() >= component.getY() + component.getHeight() ){
			return false;
		} else {
			return true;
		}
	}

	protected boolean isMovingDown(final GameComponent component){
		int i = 0;
		if(component instanceof GameObject) {
			i = 5;
		}
		if(this.getX() + this.getWidth() < component.getX() + i || this.getX()  > component.getX() + component.getWidth() - i || this.getY() + this.getHeight() < component.getY() || this.getY() +this.getHeight() > component.getY() + i){
			return false;
		} else {
			return true;
		}
	}
	
	protected boolean isMovingUp(final GameComponent component){
		if(this.getX() + this.getWidth() < component.getX() + 5 || this.getX()  > component.getX() + component.getWidth() - 5 || this.getY() < component.getY() + component.getHeight() || this.getY() > component.getY()+ component.getHeight() + 5) {
			return false;
		} else {
			return true;
		}				
	}

}
