package GameComponent.Character;

import java.awt.Image;

import GameComponent.Component;
import GameComponent.GameComponent;
import GameComponent.GameObject.GameObject;
import GameComponent.GameObject.GameObjectImpl;

public interface Character extends GameComponent{

	void setAlive(boolean isAlive);

	void setMoving(boolean isMoving);

	void setMovingRight(boolean isMovingRight);

	void setCounter(int counter);

	int getCounter();

	boolean isAlive();

	boolean isMoving();

	boolean isMovingRight();

	Image changeImage(String name, int frequency);

	boolean isCloseToAComponent(GameComponent component);

	void contactWithComponent(GameComponent component);

//	Image changeImageAfterDeath();

}
