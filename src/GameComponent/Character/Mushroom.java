package GameComponent.Character;

import java.awt.Image;

import javax.swing.ImageIcon;

import GameComponent.GameComponent;
import GameComponent.GameObject.GameObjectImpl;

public class Mushroom extends DeadlyCharacter{

	public Mushroom(final int x, final int y) {
		super(x, y,27 , 30, "/imagine/funghiAD.png");
		
		Thread mushroomThread = new Thread(this);
		mushroomThread.start();
	}
	
  	public Image changeImageAfterDeath(){
		String pathImage;
		ImageIcon icon;
		Image image;
        if(this.isMovingRight() == true){
        	pathImage = "/imagine/funghiED.png";
        } else {
        	pathImage = "/imagine/funghiEG.png";
        }
        icon = new ImageIcon(getClass().getResource(pathImage));
        image = icon.getImage();
		return image;
  	}

}
