package GameComponent.Character;

import GameComponent.GameComponent;

import javax.swing.*;
import java.awt.*;

public abstract class DeadlyCharacter extends CharacterImpl implements Runnable {

    private Image image;
    private ImageIcon icon;

    private final int PAUSE = 15;
    private int index;

    public DeadlyCharacter(int x, int y, final int width, final int heigt, final String pathImage) {
        super(x, y, width, heigt);
        super.setMovingRight(true);
        super.setMoving(true);
        this.index = 1;
        this.icon = new ImageIcon(getClass().getResource(pathImage));
        this.image = icon.getImage();
    }

    private Image getImage() {
        return image;
    }

    @Override
    public void shift(){
        if(super.isMovingRight() == true){
            this.index =1;
        } else {
            this.index = -1;
        }
        super.setX(super.getX()+this.index);
    }

    @Override
    public void run() {
        try{
            Thread.sleep(20);
        } catch(InterruptedException e){

        }
        while(true){
            if(this.isAlive() == true){
                this.shift();
                try{
                    Thread.sleep(PAUSE);
                } catch(InterruptedException e){

                }
            }
        }
    }

    public void contactWithComponent(final GameComponent component){
        if(super.isMovingRight(component) == true && this.isMovingRight() == true){
            super.setMovingRight(false);
            this.index = -1;
        } else if (super.isMovingLeft(component)==true && this.isMovingRight() ==false){
            super.setMovingRight(true);
            this.index = 1;
        }
    }

    public abstract Image changeImageAfterDeath();
}
