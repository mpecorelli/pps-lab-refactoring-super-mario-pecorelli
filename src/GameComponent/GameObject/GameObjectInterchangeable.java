package GameComponent.GameObject;

import java.awt.*;

public interface GameObjectInterchangeable extends GameObject {

    Image changeImage();

}
