package GameComponent.GameObject;

public class Brick extends GameObjectImpl {
	
	public Brick(final int x, final int y) {
		super(x, y, 30, 30, "/imagine/Blocco.png");
	}

}
