package GameComponent.GameObject;

import GameComponent.GameComponent;

import javax.swing.*;
import java.awt.*;

public interface GameObject extends GameComponent{

    Image getImage();

    ImageIcon getIcon();

    void setImage(Image image);

    void setIcon(ImageIcon icon);

}