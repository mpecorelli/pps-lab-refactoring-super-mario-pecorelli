package GameComponent.GameObject;

import java.awt.Image;

import javax.swing.ImageIcon;

import GameComponent.Component;
import Gioco.Main;

public class GameObjectImpl extends Component implements GameObject{

	private Image image;
	private ImageIcon icon;
	
	public GameObjectImpl(final int x, final int y, final int width, final int height, final String pathImage){
		super(x, y, width, height);

		setIcon(new ImageIcon(getClass().getResource(pathImage)));
		this.icon = getIcon();
		setImage(this.icon.getImage());
	}

	@Override
	public Image getImage() {
		return this.image;
	}

	@Override
	public ImageIcon getIcon() {
		return this.icon;
	}

	@Override
	public void setImage(final Image image) {
		this.image = image;
	}

	@Override
	public void setIcon(final ImageIcon icon) {
		this.icon = icon;
	}

}
