package GameComponent.GameObject;

import java.awt.Image;

import javax.swing.ImageIcon;

public class Coin extends GameObjectImpl implements Runnable, GameObjectInterchangeable{

	private int counter;
	private final int PAUSE = 10;

	public Coin(final int x, final int y) {
		super(x, y, 30, 30, "/imagine/piece1.png");
	}

	@Override
	public Image changeImage(){
		ImageIcon icon;
		Image image;
		String pathImage;
		this.counter++;
		if(this.counter / 100 == 0){
			pathImage= "/imagine/piece1.png";
		} else {
			pathImage = "/imagine/piec.png";
		}
		if(this.counter == 200 ) {
			this.counter = 0;
		}
		icon = new ImageIcon(getClass().getResource(pathImage));
		image =icon.getImage();
		return image;
	}

	@Override
	public void run() {
		try{
			Thread.sleep(10);
		} catch(InterruptedException e){

		}
		while(true){
			this.changeImage();
			try{
				Thread.sleep(PAUSE);
			} catch(InterruptedException e){

			}
		}
		
	}

}
