package GameComponent;

public interface GameComponent {

    int getWidth();

    int getHeight();

    int getX();

    int getY();

    void setX(int x);

    void setY(int y);

    void shift();

}
